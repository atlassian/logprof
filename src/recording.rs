use chrono::UTC;
use std::io::{self, BufRead, BufWriter, Write};

pub fn record<R: BufRead, W: Write>(input: &mut R, output: &mut BufWriter<W>, time_format: &str) -> io::Result<()> {
    for line_result in input.lines() {
        let line = try!(line_result);
        try!(stamp_line(output, time_format, line.as_str()));
    };
    // terminating timestamp so we can record when input was finished (i.e. final line time taken)
    try!(stamp_line(output, time_format, "logprof detected input EOF"));

    Ok(())
}

#[inline]
fn stamp_line<W: Write>(output: &mut BufWriter<W>, time_format: &str, s: &str) -> io::Result<()> {
    writeln!(output, "{} {}", UTC::now().format(time_format), s)
        .and_then(|_| output.flush())
}
