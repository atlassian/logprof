extern crate chrono;
extern crate docopt;
extern crate env_logger;
#[macro_use]
extern crate log;
extern crate regex;
extern crate rustc_serialize;

mod recording;
mod reporting;

use docopt::Docopt;
use regex::Regex;
use std::fs::File;
use std::io::{self, BufReader, BufWriter, BufRead, Write};

const USAGE: &'static str = r"
logprof - find the slow stages of an operation based on its log outputs.

Usage:
    logprof record [--time-format FORMAT] [ <input> ]
    logprof report [--time-format FORMAT] [--parse-with PATTERN] [--display-min MIN] [--graph-type TYPE] [ <input> ]
    logprof (-h | --help | --version)

Options:
    <input>     Override the input source (which is STDIN by default).
                If this is specified as the single character '-', then STDIN will still be used.
                * In record mode, this is generally not useful and is only included for niche use
                  cases such as reading from pipes.
                * In report mode, this is useful if you have pre-existing logs which already have
                  timestamps in them from a source other than logprof record. In this case, you
                  probably want to specify --time-format and --parse-with as well (see examples).

    --time-format=FORMAT
                Override the timestamp formatting. See
                https://lifthrasiir.github.io/rust-chrono/chrono/format/strftime/index.html
                for possible format options.
                [default: %Y-%m-%dT%H:%M:%S%.fZ]

    --parse-with=REGEX
                Override the regular expression used to extract the timestamp and log.
                The regular expression must have 2 capture groups, time and log, which should
                capture the timestamp and log line content respectively. If the regular expression
                matches, each capture group must also match exactly once.
                Only applicable when reporting.
                [default: ^(?P<time>.+T.+\.\d+Z) (?P<log>.*)$]

    --display-min=MIN
                Only display lines which take this much time relative to other lines.
                For example, --display-min=5 would show only those log lines which take at least
                half as long as the longest log line, and MIN=10 would show only the line that took
                the longest.
                Only applicable when reporting.
                Valid values: a number in the range of [0,10].
                [default: 0]

    --graph-type=TYPE
                Configure the type of graph that should be displayed.
                Only applicable when reporting.
                Valid values: linear, sqrt, log
                [default: linear]

    -h --help   Show this screen.

    --version   Show version.

Examples:
    mvn clean install -DskipTests | logprof record | logprof report | less -S
        See which bits are slow in the maven build (waits for whole build to finish first).

    mvn clean install -DskipTests | logprof record | tee logprof.out
        Save recording, which is useful when analysing it multiple times.

    logprof report logprof.out | less -S
        Report on saved recording.

    logprof report <( grep -- '--- ' < logprof.out) | less -S
        Report on maven plugin execution times, which are conveniently delineated with '--- '.

    cargo build --release -j 1 | logprof record | tee logprof.out | logprof report | less -S
        See which bits of a rust compile via Cargo are slowest (note -j flag to use single thread).

    logprof report --time-format '%d-%b-%Y %H:%M:%S' --parse-with \
            '^\w+\s+(?P<time>\d\d-\w{3}-\d{4} \d\d:\d\d:\d\d)\s+?(?P<log>.*)' \
            bamboo-job.log
        Use a custom time format and parser regex to report on Bamboo logs, which look like:
        simple  10-Mar-2016 00:28:12    Starting task do_a_barrel_roll
";

#[derive(Debug, RustcDecodable)]
struct Args {
    cmd_record: bool,
    cmd_report: bool,
    arg_input: Option<String>,
    flag_time_format: String,
    flag_parse_with: String,
    flag_display_min: u8,
    flag_graph_type: reporting::GraphType,
}

fn main() {
    env_logger::init().expect("failed to initialise logprof's logging system");

    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d
                                .help(true)
                                .version(Some(env!("CARGO_PKG_VERSION").to_string()))
                                .decode())
                            .unwrap_or_else(|e| e.exit());
    debug!("Command line args are: {:#?}", args);

    if let Err(err) = execute(args) {
        writeln!(&mut io::stderr(), "Error: {}", err).expect("Fatal error while reporting error!");
    }
}

fn execute(args: Args) -> io::Result<()> {
    let stdin = io::stdin();
    let mut input: Box<BufRead> = match args.arg_input {
        Some(ref filename) if filename != "-" => {
            let file = try!(File::open(filename).map_err(|err| io::Error::new(io::ErrorKind::InvalidInput,
                format!("Failed to open input '{}' for reading: {}", filename, err))
            ));
            Box::new(BufReader::new(file))
        },
        _ => Box::new(stdin.lock())
    };

    let stdout = io::stdout();
    let mut output = BufWriter::new(stdout.lock());

    if args.cmd_record {
        recording::record(&mut input, &mut output, args.flag_time_format.as_ref())
    } else if args.cmd_report {
        let options = reporting::ReportOptions {
            time_format: args.flag_time_format.clone(),
            parser_regex: try!(build_parser_regex_from_parse_with(&args.flag_parse_with.as_str())
                .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput,
                    format!("'{}' argument provided to --parser-regex is invalid: {}", args.flag_parse_with.as_str(), err))
                )),
            display_min: if args.flag_display_min > reporting::MAX_GRAPH_WIDTH {
                return Err(io::Error::new(io::ErrorKind::InvalidInput,
                    format!("'{}' argument provided to --display-min is greater than {}",
                        args.flag_display_min, reporting::MAX_GRAPH_WIDTH)))
            } else { args.flag_display_min },
            graph_type: args.flag_graph_type,
        };

        reporting::report(&mut input, &mut output, &options)
    } else {
        // should by stopped by docopt.rs's arg parsing, but we'll handle it nicely just in case
        Err(io::Error::new(io::ErrorKind::InvalidInput, "Subcommand should have been specified"))
    }
}

fn build_parser_regex_from_parse_with(parse_with: &str) -> Result<Regex, String> {
    match Regex::new(parse_with) {
        Err(err) => Err(format!(
            "is not a valid regular expression: {:?}",
            err)),
        Ok(regex) => {
            {
                let capture_names = regex.capture_names().filter_map(|c| c).collect::<Vec<_>>();
                if capture_names.len() != 2 {
                    return Err(format!(
                        "has {} named captures (named: {:?}) but requires 2 (named {} and {})",
                        capture_names.len(), capture_names, reporting::TIME_CAPTURE_NAME, reporting::LOG_CAPTURE_NAME))
                } else if !capture_names.contains(&reporting::TIME_CAPTURE_NAME) || !capture_names.contains(&reporting::LOG_CAPTURE_NAME) {
                    return Err(format!("correctly has 2 named captures but they are named {:?} rather than [{}, {}]",
                        capture_names, reporting::TIME_CAPTURE_NAME, reporting::LOG_CAPTURE_NAME))
                }
            } // make sure regex is no longer borrowed
            Ok(regex)
        },
    }
}
